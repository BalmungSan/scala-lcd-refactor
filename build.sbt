//project settings
name := "Scala LCD Refactor"
version := "1.0.0"
scalaVersion := "2.12.3"
scalacOptions ++= Seq("-deprecation", "-feature")

//test framework
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
