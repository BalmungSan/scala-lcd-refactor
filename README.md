# Scala LCD Refactor
Solution to the exercise [LCD Refactor](https://github.com/pslcorp/lcdrefactor) of [**_PSL_**](http://www.psl.com.co/)

# Author
Luis Miguel Mejía Suárez (_BalmungSan_)

# Version
1.0.0 - (31/08/2017)

## Application Usage
This application is written in [**Scala**](http://scala-lang.org/) _2.12_ and using [**SBT**](http://www.scala-sbt.org/) _1.0.1_

### Run the Application

    $ sbt run
    Welcome to LCD Digit Printer
    This program prints the digits specified in each input line as LCD Digits
    Each input line must be of the form { [size], [digit]+, [spaces]?}
    The size must be in the range [1, 10]
    Spaces is the number of spaces betwee two digits, is optional and must be in the range [0, 5]
    To end the execution input the line 0,0
    >
    
### Run the unit tests

    $ sbt test