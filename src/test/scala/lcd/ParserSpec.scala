package lcd

//test base class
import org.scalatest.FlatSpec

class ParserSpec extends FlatSpec {
  "The Parser" must "return an empty string for the input 0.0" in {
    assert(Parser("0,0").isEmpty)
  }

  it must "allow the input of the spaces variable" in {
    assert(!Parser("2,34,2").isEmpty)
  }

  it must "throw IllegalArgumentException if the size is not in the range [1, 10]" in {
    assertThrows[IllegalArgumentException] {
      Parser("0,9")
    }
    assertThrows[IllegalArgumentException] {
      Parser("11,9")
    }
  }

  it must "throw IllegalArgumentException if spaces is not in the range [0, 5]" in {
    assertThrows[IllegalArgumentException] {
      Parser("2,34,6")
    }
  }

  it must "throw IllegalArgumentException if the input line does not conform to the format" in {
    assertThrows[IllegalArgumentException] {
      Parser("-1,0")
    }
    assertThrows[IllegalArgumentException] {
      Parser("1,-5")
    }
    assertThrows[IllegalArgumentException] {
      Parser("A,0")
    }
    assertThrows[IllegalArgumentException] {
      Parser("1,A")
    }
    assertThrows[IllegalArgumentException] {
      Parser("0.0")
    }
    assertThrows[IllegalArgumentException] {
      Parser("2,34,-1")
    }
  }
}
