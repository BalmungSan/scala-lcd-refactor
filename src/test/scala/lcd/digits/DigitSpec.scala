package lcd.digits

import org.scalatest.FlatSpec

class DigitSpec extends FlatSpec {
  "For every Digit its rows and columns" must "follow the size rules" in {
    for (d <- 0 to 9) { //digit
      for(s <- 1 to 10) { //size
        val digit = Digit(d, s)
        assert(digit.rows.size === ((2 * s) + 3))
        for (row <- digit.rows) {
          assert(row.size === (s + 2))
        }
      }
    }
  }

  "The Digit factory" must "throw IllegalArgumentException if the size is not in the range [1, 10]"  in {
    assertThrows[IllegalArgumentException] {
      Digit(5, 0)
    }
    assertThrows[IllegalArgumentException] {
      Digit(5, 11)
    }
    assertThrows[IllegalArgumentException] {
      Digit(5, -1)
    }
  }

  "The Digit factory" must "throw IllegalArgumentException if the digit is not in the range [0, 9]"  in {
    assertThrows[IllegalArgumentException] {
      Digit(10, 5)
    }
    assertThrows[IllegalArgumentException] {
      Digit(-1, 5)
    }
  }

  "The Digit Zero" must "print correctly" in {
    val zero = Digit(0, 2)
    val zeroLCD = " -- \n" +
                  "|  |\n" +
                  "|  |\n" +
                  "    \n" +
                  "|  |\n" +
                  "|  |\n" +
                  " -- "
    assert(zero.toString === zeroLCD)
  }

  "The Digit One" must "print correctly" in {
    val one = Digit(1, 2)
    val oneLCD = "    \n" +
                 "   |\n" +
                 "   |\n" +
                 "    \n" +
                 "   |\n" +
                 "   |\n" +
                 "    "
    assert(one.toString === oneLCD)
  }

  "The Digit Two" must "print correctly" in {
    val two = Digit(2, 2)
    val twoLCD = " -- \n" +
                 "   |\n" +
                 "   |\n" +
                 " -- \n" +
                 "|   \n" +
                 "|   \n" +
                 " -- "
    assert(two.toString === twoLCD)
  }

  "The Digit Three" must "print correctly" in {
    val three = Digit(3, 2)
    val threeLCD = " -- \n" +
                   "   |\n" +
                   "   |\n" +
                   " -- \n" +
                   "   |\n" +
                   "   |\n" +
                   " -- "
    assert(three.toString === threeLCD)
  }

  "The Digit Four" must "print correctly" in {
    val four = Digit(4, 2)
    val fourLCD = "    \n" +
                  "|  |\n" +
                  "|  |\n" +
                  " -- \n" +
                  "   |\n" +
                  "   |\n" +
                  "    "
    assert(four.toString === fourLCD)
  }

  "The Digit Five" must "print correctly" in {
    val five = Digit(5, 2)
    val fiveLCD = " -- \n" +
                  "|   \n" +
                  "|   \n" +
                  " -- \n" +
                  "   |\n" +
                  "   |\n" +
                  " -- "
    assert(five.toString === fiveLCD)
  }

  "The Digit Six" must "print correctly" in {
    val six = Digit(6, 2)
    val sixLCD = " -- \n" +
                 "|   \n" +
                 "|   \n" +
                 " -- \n" +
                 "|  |\n" +
                 "|  |\n" +
                 " -- "
    assert(six.toString === sixLCD)
  }

  "The Digit Seven" must "print correctly" in {
    val seven = Digit(7, 2)
    val sevenLCD = " -- \n" +
                   "   |\n" +
                   "   |\n" +
                   "    \n" +
                   "   |\n" +
                   "   |\n" +
                   "    "
    assert(seven.toString === sevenLCD)
  }

  "The Digit Eight" must "print correctly" in {
    val eight = Digit(8, 2)
    val eightLCD = " -- \n" +
                   "|  |\n" +
                   "|  |\n" +
                   " -- \n" +
                   "|  |\n" +
                   "|  |\n" +
                   " -- "
    assert(eight.toString === eightLCD)
  }

  "The Digit Nine" must "print correctly" in {
    val nine = Digit(9, 2)
    val nineLCD = " -- \n" +
                  "|  |\n" +
                  "|  |\n" +
                  " -- \n" +
                  "   |\n" +
                  "   |\n" +
                  " -- "
    assert(nine.toString === nineLCD)
  }
}
