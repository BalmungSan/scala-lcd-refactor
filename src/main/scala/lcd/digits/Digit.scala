package lcd.digits

/** Abstract class that represents any digit in the LCD display */
sealed abstract class Digit(val size: Int) {
  //Horizontal Character
  private[this] val HC = "-"
  //Vertical Character
  private[this] val VC = "|"

  //rows types
  protected val superiorRow: String
  protected val centerURows: String
  protected val centerRow: String
  protected val centerLRows: String
  protected val inferiorRow: String

  //common structures in all digits
  protected val emptyRow      = " " * (size + 2)        //"     "
  protected val horizontalRow = " " + (HC * size) + " " //" --- "
  protected val squareRow     = VC + (" " * size) + VC  //"|   |"
  protected val leftRow       = VC + (" " * (size + 1)) //"|    "
  protected val rightRow      = (" " * (size + 1)) + VC //"    |"

  //rows idx
  private[this] val totalRows = (2 * size) + 3
  private[this] val middleRow = totalRows / 2
  private[this] val lastRow   = totalRows - 1

  /** The string representation of this digit as a collection of rows */
  lazy val rows: Seq[String] = (0 until totalRows) map {
    case 0                     => superiorRow //superior vertical edge
    case x if (x == lastRow)   => inferiorRow //inferior vertical edge
    case x if (x == middleRow) => centerRow   //center row
    case x if (x < middleRow)  => centerURows //all rows between the superior row and the center row
    case x if (x > middleRow)  => centerLRows //all rows between the center row and the lower row
  }

  /** Print this digit */
  override def toString(): String = rows.mkString("\n")
}


/** Factory of Digits */
object Digit {
  /** Creates a new Digit object for the specified digit and size
   *
   * @param digit the digit to instanciate, must be in the range [0, 9]
   * @param size the size of the digit, must be in the range [1, 10]
   *
   * @throws java.lang.illelgalArgumentException if the provided digit or size
   * are not in their valid ranges
   */
  @throws(classOf[IllegalArgumentException])
  def apply(digit: Int, size: Int): Digit = {
    require(digit >= 0 && digit <= 9, "only digits accepted")
    require(size >= 1 && size <= 10, "size must be in range [1, 10]")

    digit match {
      case 0 => new Zero(size)
      case 1 => new One(size)
      case 2 => new Two(size)
      case 3 => new Three(size)
      case 4 => new Four(size)
      case 5 => new Five(size)
      case 6 => new Six(size)
      case 7 => new Seven(size)
      case 8 => new Eight(size)
      case 9 => new Nine(size)
    }
  }

  /** Digit 0 */
  private case class Zero(override val size: Int) extends Digit(size) {
    override val superiorRow = horizontalRow // --
    override val centerURows = squareRow     //|  |
    override val centerRow   = emptyRow      //
    override val centerLRows = squareRow     //|  |
    override val inferiorRow = horizontalRow // --
  }

  /** Digit 1 */
  private case class One(override val size: Int) extends Digit(size) {
    override val superiorRow = emptyRow //
    override val centerURows = rightRow //  |
    override val centerRow   = emptyRow //
    override val centerLRows = rightRow //  |
    override val inferiorRow = emptyRow //
  }

  /** Digit 2 */
  private case class Two(override val size: Int) extends Digit(size) {
    override val superiorRow = horizontalRow // --
    override val centerURows = rightRow      //   |
    override val centerRow   = horizontalRow // --
    override val centerLRows = leftRow       //|
    override val inferiorRow = horizontalRow // --
  }

  /** Digit 3 */
  private case class Three(override val size: Int) extends Digit(size) {
    override val superiorRow = horizontalRow // --
    override val centerURows = rightRow      //   |
    override val centerRow   = horizontalRow // --
    override val centerLRows = rightRow      //   |
    override val inferiorRow = horizontalRow // --
  }

  /** Digit 4 */
  private case class Four(override val size: Int) extends Digit(size) {
    override val superiorRow = emptyRow      //
    override val centerURows = squareRow     //|  |
    override val centerRow   = horizontalRow // --
    override val centerLRows = rightRow      //   |
    override val inferiorRow = emptyRow      //
  }

  /** Digit 5 */
  private case class Five(override val size: Int) extends Digit(size) {
    override val superiorRow = horizontalRow // --
    override val centerURows = leftRow       //|
    override val centerRow   = horizontalRow // --
    override val centerLRows = rightRow      //   |
    override val inferiorRow = horizontalRow // --
  }

  /** Digit 6 */
  private case class Six(override val size: Int) extends Digit(size) {
    override val superiorRow = horizontalRow // --
    override val centerURows = leftRow       //|
    override val centerRow   = horizontalRow // --
    override val centerLRows = squareRow     //|  |
    override val inferiorRow = horizontalRow // --
  }

  /** Digit 7 */
  private case class Seven(override val size: Int) extends Digit(size) {
    override val superiorRow = horizontalRow // --
    override val centerURows = rightRow      //   |
    override val centerRow   = emptyRow      //
    override val centerLRows = rightRow      //   |
    override val inferiorRow = emptyRow      //
  }

  /** Digit 8 */
  private case class Eight(override val size: Int) extends Digit(size) {
    override val superiorRow = horizontalRow // --
    override val centerURows = squareRow     //|  |
    override val centerRow   = horizontalRow // --
    override val centerLRows = squareRow     //|  |
    override val inferiorRow = horizontalRow // --
  }

  /** Digit 9 */
  private case class Nine(override val size: Int) extends Digit(size) {
    override val superiorRow = horizontalRow // --
    override val centerURows = squareRow     //|  |
    override val centerRow   = horizontalRow // --
    override val centerLRows = rightRow      //   |
    override val inferiorRow = horizontalRow // --
  }
}
