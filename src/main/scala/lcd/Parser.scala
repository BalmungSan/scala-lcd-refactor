package lcd

//imports
import lcd.digits.Digit
import scala.util.matching.Regex

/** Parser of the user input lines */
object Parser {
  //regex for the input lines
  private val input = raw"(\d+),(\d+),?(\d{1})?".r

  /** Parse a line
   *
   * This method attempts to parse a line with the following format { [size],[digit]+, [spaces]? }
   * and returns a string that contains the LCD form of the digits
   * in the input line with the specified size and number of spaces
   *
   * @param line the line to parse
   * @returns an empty string if the input line is the stop command,
   * otherwise a string with the parsed digits
   *
   * @note the size must be in range [1, 10]
   * @note the number of spaces must be in range [0, 5]
   * @note the input line "0,0" means finalize the execution
   *
   * @throws java.lang.IllegalArgumentException if the input string
   * does not conform to the input format
   */
  @throws(classOf[IllegalArgumentException])
  def apply(line: String): String = {
    try {
      line match {
        case input("0", "0", _) => "" //end execution
        case input(s, digits, sp) => {
          val size = s.toInt
          val spaces = if (sp == null) 1 else sp.toInt
          if (spaces < 0 || spaces > 5) throw new IllegalArgumentException("spaces must be in range [0, 5]")
          val lcdDigits = digits map { digit => Digit(digit.asDigit, size).rows }
          lcdDigits.transpose.map(row => row.mkString(" " * spaces)).mkString("\n")
        }
      }
    } catch {
      case me: MatchError => {
        throw new IllegalArgumentException("the input line does not conform to the input format")
      }
    }
  }
}
