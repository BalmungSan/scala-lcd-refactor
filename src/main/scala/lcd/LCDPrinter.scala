package lcd

/** Application Entry Point */
object LCDPrinter extends App {
  import Console.{GREEN, RED, RESET}

  //main
  println("""Welcome to LCD Digit Printer
            |This program prints the digits specified in each input line as LCD Digits
            |Each input line must be of the form { [size], [digit]+, [spaces]?}
            |The size must be in the range [1, 10]
            |Spaces is the number of spaces betwee two digits, is optional and must be in the range [0, 5]
            |To end the execution input the line 0,0""".stripMargin)

  var run = true
  while (run) {
    //ask the user for an input
    print("> ")
    val line = scala.io.StdIn.readLine()

    //try parse the user input
    try {
      val result = Parser(line)

      //check if the user request to stop the execution
      if (result.isEmpty) run = false
      //if not print the LCD Message
      else println(result)
    } catch {
      case ex: IllegalArgumentException => {
        Console.err.println(s"""${RESET}${RED}Error: Bad User input: ${line}
                               |Caused by:${ex.getMessage()}${RESET}""".stripMargin)
      }
    }
  }
}
